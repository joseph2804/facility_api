﻿using Dapper;
using GasStationTraining.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace facility_web_api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TatemonoController : ControllerBase
    {
        private readonly IDapper _dapper;
        public TatemonoController(IDapper dapper)
        {
            _dapper = dapper;
        }

        #region get tatemono -----------
        [EnableCors("AllowAll")]
        public Dictionary<string, List<object>> GetAllTatemonos()
        {
            List<string> ListTableTatemono = new List<string>
            (new string[]
                {
                    "TATEMONO_CHOUSA_JOUTAI",
                    "TATEMONO_CHOUSA_KOUMOKU",
                    "TATEMONO_KIHON",
                    "TATEMONO_MULTI_CHECK",
                    "TATEMONO_NENKAN_KEIKAKU",
                    "TATEMONO_NENKAN_KEIKAKU_GATSU",
                    "TATEMONO_SHITSUGAIKI",
                    "TATEMONO_SHITSUNAIKI",
                    "TATEMONO_SHOSHITSU"
                }
            );
            Dictionary<string, List<object>> dic = new Dictionary<string, List<object>>();

            foreach (var tb in ListTableTatemono)
            {
                string query = " ";

                query += " SELECT ";
                query += " * ";
                query += " FROM ";
                query += $" {tb} ";

                using (var db = _dapper.GetDbconnection())
                {
                    var result = db.Query<Object>(query).ToList();
                    dic.Add(tb, result);
                }
            }

            return dic;
        }

        #endregion
    }
}
