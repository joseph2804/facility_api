﻿using Dapper;
using facility_web_api.Models;
using facility_web_api.Utils;
using GasStationTraining.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace facility_web_api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SyncDataController : ControllerBase
    {
        private readonly IDapper _dapper;
        public getEtcValue getEtc = new getEtcValue() ;
        public SyncDataController(IDapper dapper)
        {
            _dapper = dapper;
        }
        [HttpPost]
        public bool SyncRireki([FromBody] List<CHOUSA_KENCHIKU_RIREKI > list)
        {
            foreach (var item in list)
            {
                var SELECT = "SELECT * FROM CHOUSA_KENCHIKU_RIREKI     ";
                SELECT += $"WHERE TATEMONO_ID = {item.TATEMONO_ID }     ";
                SELECT += $"AND NO_CHOUSA = {item.NO_CHOUSA}    ";
                var chousaItem = _dapper.Get<CHOUSA_KENCHIKU_RIREKI>(SELECT, null);
                DynamicParameters param_list = new DynamicParameters();
                if (chousaItem != null)
                {
                    var QueryUpdate = "UPDATE CHOUSA_KENCHIKU_RIREKI   ";
                    QueryUpdate += "SET ";
                    QueryUpdate += $"   TENKEN_SHUBETSU = @TENKEN_SHUBETSU ,    ";
                    QueryUpdate += $"   NENGAPPI_CHOUSA = @NENGAPPI_CHOUSA,    ";
                    QueryUpdate += $"   NAME_CHOUSAIN =  @NAME_CHOUSAIN,   ";
                    QueryUpdate += $"   CODE_CHOUSA_KEKKA = @CODE_CHOUSA_KEKKA,   ";
                    QueryUpdate += $"   COMMENT_CHOUSA_KEKKA = @COMMENT_CHOUSA_KEKKA,   ";
                    QueryUpdate += $"   TAIOU_HOUSHIN = @TAIOU_HOUSHIN,   ";
                    QueryUpdate += $"   KAKAKU_MITSUMORI = @KAKAKU_MITSUMORI,   ";
                    QueryUpdate += $"   TENKEN_YOUTEI_NENGATSU = @TENKEN_YOUTEI_NENGATSU   ";
                    QueryUpdate += "WHERE";
                    QueryUpdate += $"   TATEMONO_ID = @TATEMONO_ID   ";
                    QueryUpdate += $"   AND NO_CHOUSA = @NO_CHOUSA   ";

                    param_list.Add("TENKEN_SHUBETSU", item.TENKEN_SHUBETSU);
                    param_list.Add("NENGAPPI_CHOUSA", item.NENGAPPI_CHOUSA);
                    param_list.Add("NAME_CHOUSAIN", item.NAME_CHOUSAIN);
                    param_list.Add("CODE_CHOUSA_KEKKA", item.CODE_CHOUSA_KEKKA);
                    param_list.Add("COMMENT_CHOUSA_KEKKA", item.COMMENT_CHOUSA_KEKKA);
                    param_list.Add("TAIOU_HOUSHIN", item.TAIOU_HOUSHIN);
                    param_list.Add("KAKAKU_MITSUMORI", item.KAKAKU_MITSUMORI);
                    param_list.Add("KAKAKU_MITSUMORI", item.KAKAKU_MITSUMORI);
                    param_list.Add("TENKEN_YOUTEI_NENGATSU", item.TENKEN_YOUTEI_NENGATSU);
                    param_list.Add("TATEMONO_ID", item.TATEMONO_ID);
                    param_list.Add("NO_CHOUSA", item.NO_CHOUSA);

                    return _dapper.Insert<bool>(QueryUpdate, param_list);
                }
                else
                {
                    var QueryInsert = "INSERT INTO CHOUSA_KENCHIKU_RIREKI  ";
                    QueryInsert += "    (TATEMONO_ID, NO_CHOUSA, TENKEN_SHUBETSU, NENGAPPI_CHOUSA, NAME_CHOUSAIN, CODE_CHOUSA_KEKKA, COMMENT_CHOUSA_KEKKA, TAIOU_HOUSHIN, KAKAKU_MITSUMORI, TENKEN_YOUTEI_NENGATSU )    ";
                    QueryInsert += "VALUES";
                    QueryInsert += $"    (@TATEMONO_ID, @NO_CHOUSA, @TENKEN_SHUBETSU, @NENGAPPI_CHOUSA, @NAME_CHOUSAIN, @CODE_CHOUSA_KEKKA, @COMMENT_CHOUSA_KEKKA, @TAIOU_HOUSHIN, @KAKAKU_MITSUMORI, @TENKEN_YOUTEI_NENGATSU)  ";

                    param_list.Add("TATEMONO_ID", item.TATEMONO_ID);
                    param_list.Add("NO_CHOUSA", item.NO_CHOUSA);
                    param_list.Add("TENKEN_SHUBETSU", item.TENKEN_SHUBETSU);
                    param_list.Add("NENGAPPI_CHOUSA", item.NENGAPPI_CHOUSA);
                    param_list.Add("NAME_CHOUSAIN", item.NAME_CHOUSAIN);
                    param_list.Add("CODE_CHOUSA_KEKKA", item.CODE_CHOUSA_KEKKA);
                    param_list.Add("COMMENT_CHOUSA_KEKKA", item.COMMENT_CHOUSA_KEKKA);
                    param_list.Add("TAIOU_HOUSHIN", item.TAIOU_HOUSHIN);
                    param_list.Add("KAKAKU_MITSUMORI", item.KAKAKU_MITSUMORI);
                    param_list.Add("TENKEN_YOUTEI_NENGATSU", item.TENKEN_YOUTEI_NENGATSU);

                    return _dapper.Insert<bool>(QueryInsert, param_list);
                }
            }

            return false;
        }
        [HttpPost]
        public bool SyncBui([FromBody] List<CHOUSA_KENCHIKU_BUI> list)
        {
            foreach (var item in list)
            {
                var SELECT = "SELECT * FROM CHOUSA_KENCHIKU_BUI     ";
                SELECT += $"WHERE TATEMONO_ID = {item.TATEMONO_ID }     ";
                SELECT += $"AND NO_CHOUSA_KOUMOKU = {item.NO_CHOUSA_KOUMOKU}    ";
                SELECT += $"AND NO_CHOUSA = {item.NO_CHOUSA}    ";
                var chousaItem = _dapper.Get<CHOUSA_KENCHIKU_BUI>(SELECT, null);
                DynamicParameters param_list = new DynamicParameters();
                if (chousaItem != null)
                {
                    var QueryUpdate = "UPDATE CHOUSA_KENCHIKU_BUI   ";
                    QueryUpdate += "SET ";
                    QueryUpdate += $"   NO_JOUTAI = @NO_JOUTAI,    ";
                    QueryUpdate += $"   SHASHIN = @SHASHIN,    ";
                    QueryUpdate += $"   BIKO = @BIKO   ";
                    QueryUpdate += "WHERE";
                    QueryUpdate += $"   TATEMONO_ID = @TATEMONO_ID   ";
                    QueryUpdate += $"   AND NO_CHOUSA_KOUMOKU = @NO_CHOUSA_KOUMOKU    ";
                    QueryUpdate += $"   AND NO_CHOUSA = @NO_CHOUSA    ";

                    param_list.Add("NO_JOUTAI", item.NO_JOUTAI);
                    param_list.Add("SHASHIN", item.SHASHIN);
                    param_list.Add("BIKO", item.BIKO);
                    param_list.Add("TATEMONO_ID", item.TATEMONO_ID);
                    param_list.Add("NO_CHOUSA_KOUMOKU", item.NO_CHOUSA_KOUMOKU);
                    param_list.Add("NO_CHOUSA", item.NO_CHOUSA);

                    return _dapper.Insert<bool>(QueryUpdate, param_list);
                }
                else
                {
                    var QueryInsert = "INSERT INTO CHOUSA_KENCHIKU_BUI  ";
                    QueryInsert += "    (TATEMONO_ID, NO_CHOUSA, NO_NENKAN_KEIKAKU, NO_CHOUSA_KOUMOKU, NO_JOUTAI, SHASHIN, BIKO)    ";
                    QueryInsert += "VALUES";
                    QueryInsert += $"    (@TATEMONO_ID, @NO_CHOUSA, @NO_NENKAN_KEIKAKU, @NO_CHOUSA_KOUMOKU, @NO_JOUTAI, @SHASHIN, @BIKO)";

                    param_list.Add("TATEMONO_ID", item.TATEMONO_ID);
                    param_list.Add("NO_CHOUSA", item.NO_CHOUSA);
                    param_list.Add("NO_NENKAN_KEIKAKU", item.NO_NENKAN_KEIKAKU);
                    param_list.Add("NO_CHOUSA_KOUMOKU", item.NO_CHOUSA_KOUMOKU);
                    param_list.Add("NO_JOUTAI", item.NO_JOUTAI);
                    param_list.Add("SHASHIN", item.SHASHIN);
                    param_list.Add("BIKO", item.BIKO);

                    return _dapper.Insert<bool>(QueryInsert, param_list);
                }
            }

            return false;
        }
        [HttpPost]
        public bool SyncKenchikuImage([FromBody] List<CHOUSA_KENCHIKU_IMAGE> list)
        {
            foreach (var item in list)
            {
                var SELECT = "SELECT * FROM CHOUSA_KENCHIKU_IMAGE     ";
                SELECT += $"WHERE TATEMONO_ID = {item.TATEMONO_ID }     ";
                SELECT += $"AND NO_UPLOAD = {item.NO_UPLOAD}    ";
                SELECT += $"AND NO_CHOUSA = {item.NO_CHOUSA}    ";
                var chousaItem = _dapper.Get<CHOUSA_KENCHIKU_IMAGE>(SELECT, null);
                DynamicParameters param_list = new DynamicParameters();
                if (chousaItem != null)
                {
                    var QueryUpdate = "UPDATE CHOUSA_KENCHIKU_IMAGE   ";
                    QueryUpdate += "SET ";
                    QueryUpdate += $"   NAME_FILE = @NAME_FILE,    ";
                    QueryUpdate += $"   NO_CHOUSA_KOUMOKU = @NO_CHOUSA_KOUMOKU   ";
                    QueryUpdate += "WHERE";
                    QueryUpdate += $"   TATEMONO_ID = @TATEMONO_ID   ";
                    QueryUpdate += $"   AND NO_UPLOAD = @NO_UPLOAD    ";
                    QueryUpdate += $"   AND NO_CHOUSA = @NO_CHOUSA    ";

                    param_list.Add("NAME_FILE", item.NAME_FILE);
                    param_list.Add("NO_CHOUSA_KOUMOKU", item.NO_CHOUSA_KOUMOKU);
                    param_list.Add("TATEMONO_ID", item.TATEMONO_ID);
                    param_list.Add("NO_UPLOAD", item.NO_UPLOAD);
                    param_list.Add("NO_CHOUSA", item.NO_CHOUSA);

                    return _dapper.Insert<bool>(QueryUpdate, param_list);
                }
                else
                {
                    var QueryInsert = "INSERT INTO CHOUSA_KENCHIKU_IMAGE  ";
                    QueryInsert += "    (TATEMONO_ID, NO_CHOUSA, NO_UPLOAD, NO_CHOUSA_KOUMOKU, NAME_FILE)    ";
                    QueryInsert += "VALUES";
                    QueryInsert += $"    (@TATEMONO_ID, @NO_CHOUSA, @NO_UPLOAD, @NO_CHOUSA_KOUMOKU, @NAME_FILE)";

                    param_list.Add("TATEMONO_ID", item.TATEMONO_ID);
                    param_list.Add("NO_CHOUSA", item.NO_CHOUSA);
                    param_list.Add("NO_UPLOAD", item.NO_UPLOAD);
                    param_list.Add("NO_CHOUSA_KOUMOKU", item.NO_CHOUSA_KOUMOKU);
                    param_list.Add("NAME_FILE", item.NAME_FILE);

                    return _dapper.Insert<bool>(QueryInsert, param_list);
                }
            }

            return false;
        }
    }
}
