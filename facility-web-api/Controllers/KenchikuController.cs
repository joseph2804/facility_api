﻿using facility_web_api.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using GasStationTraining.Services;
using Dapper;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using facility_web_api.Models.Custom;

namespace facility_web_api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class KenchikuController : ControllerBase
    {
        private readonly IDapper _dapper;
        private readonly IHostingEnvironment _hostingEnv;
        public KenchikuController(IDapper dapper, IHostingEnvironment hostingEnv)
        {
            _dapper = dapper;
            _hostingEnv = hostingEnv;
        }
        #region getKenchikus-----------
        [EnableCors("AllowAll")]
        [HttpGet]
        public Dictionary<string, List<object>> GetAllKenchikus()
        {
            List<string> ListTableKenchiku = new List<string>
            (new string[]
                {
                    "CHOUSA_KENCHIKU_BUI",
                    "CHOUSA_KENCHIKU_IMAGE",
                    "CHOUSA_KENCHIKU_RIREKI",
                    "CHOUSA_KENCHIKU_UPLOAD",
                    "KENCHIKU_BUI"
                }
            );
            Dictionary<string, List<object>> dic = new Dictionary<string, List<object>>();

            foreach (var tb in ListTableKenchiku)
            {
                string query = " ";

                query +=  " SELECT ";
                query +=  " * ";
                query +=  " FROM ";
                query += $" {tb} ";

                using (var db = _dapper.GetDbconnection())
                {
                    var result = db.Query<Object>(query).ToList();
                    dic.Add(tb , result);
                }
            }

            return dic;
        }
        [HttpPost]
        public Boolean UploadImage([FromBody]IFormFile file) 
        {
            
            if (file != null)
            {
                string newFileName = DateTime.Now.Ticks + "_" + file.FileName;
                string path = Path.Combine(_hostingEnv.ContentRootPath, "Data", newFileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                    //user.Avatar = newFileName;
                    //_db.Entry(user).Property(x => x.Avatar).IsModified = true;
                    //await _db.SaveChangesAsync();
                    //user.File = null;
                }
                return true;
            }
            return false;
        }
        #endregion
    }
}
