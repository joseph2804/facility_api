﻿using Dapper;
using GasStationTraining.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace facility_web_api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ShisetsuController : ControllerBase
    {
        private readonly IDapper _dapper;
        public ShisetsuController(IDapper dapper)
        {
            _dapper = dapper;
        }
        #region get Shisetsu -----------
        [EnableCors("AllowAll")]
        public Dictionary<string, List<object>> GetAllShisetsus()
        {
            List<string> ListTableShisetsu = new List<string>
            (new string[]
                {
                    "SHISETSU_KIHON",
                    "SHISETSU_MULTI_CHECK",
                    "SHISETSU_TATEMONO",
                    "SHISETSU_TOCHI",
                    "SHISETSU_UPLOAD"
                }
            );

            Dictionary<string, List<object>> dic = new Dictionary<string, List<object>>();

            foreach (var tb in ListTableShisetsu)
            {
                string query = " ";

                query += " SELECT ";
                query += " * ";
                query += " FROM ";
                query += $" {tb} ";

                using (var db = _dapper.GetDbconnection())
                {
                    var result = db.Query<Object>(query).ToList();
                    dic.Add(tb, result);
                }
            }
            return dic;
        }

        #endregion
    }
}
