﻿using Dapper;
using GasStationTraining.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace facility_web_api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MasterController : ControllerBase
    {
        private readonly IDapper _dapper;
        public MasterController(IDapper dapper)
        {
            _dapper = dapper;
        }
        #region getMasters-----------
        [EnableCors("AllowAll")]
        [HttpGet]
        public Dictionary<string, List<object>> GetAllMasters()
        {
            Dictionary<string, List<object>> dic = new Dictionary<string, List<object>>();
            var queryTable = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE'";
            var tables = _dapper.GetAll<string>(queryTable, null);
            foreach (var item in tables)
            {
                if (item.Contains("M_"))
                {
                    string query = " ";

                    query += " SELECT ";
                    query += " * ";
                    query += " FROM ";
                    query += $" {item} ";
                    var result = _dapper.GetAll<Object>(query, null);
                    dic.Add(item, result);
                }
            }

            return dic;
        }
        #endregion
    }
}
