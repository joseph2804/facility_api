﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace facility_web_api.Utils
{
    public class getEtcValue
    {
		public Object[] getDynamicParameters(List<DynamicParameters> param_list)
		{

			// List型からオブジェクト型に変換
			var param_count = param_list.Count;
			var sql_parameters = new Object[param_count];

			for (var i = 0; i < param_count; i++)
			{
				sql_parameters[i] = param_list[i];
			}

			return sql_parameters;
		}
	}
}
