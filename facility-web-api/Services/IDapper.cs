﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Services
{
    public interface IDapper : IDisposable
    {
        DbConnection GetDbconnection();
        T Get<T>(string sp, DynamicParameters parms);
        List<T> GetAll<T>(string sp, DynamicParameters parms);
        int Execute(string sp, DynamicParameters parms);
        T Insert<T>(string sp, DynamicParameters parms);
        T Update<T>(string sp, DynamicParameters parms);
    }
}
