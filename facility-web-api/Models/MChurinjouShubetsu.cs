﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MChurinjouShubetsu
    {
        public int CodeChurinjouShubetsu { get; set; }
        public string NameChurinjouShubetsu { get; set; }
    }
}
