﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class TatemonoChousaJoutai
    {
        public int TatemonoId { get; set; }
        public int NoNenkanKeikaku { get; set; }
        public int NoJoutai { get; set; }
        public string NameChousaJoutai { get; set; }
    }
}
