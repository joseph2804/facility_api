﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MDouroKubun
    {
        public int CodeDouroKubun { get; set; }
        public string NameDouroKubun { get; set; }
    }
}
