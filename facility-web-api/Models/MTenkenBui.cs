﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MTenkenBui
    {
        public int CodeTenkenBui { get; set; }
        public string NameTenkenBui { get; set; }
    }
}
