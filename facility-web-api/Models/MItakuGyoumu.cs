﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MItakuGyoumu
    {
        public int CodeItakuGyoumu { get; set; }
        public string NameItakuGyoumu { get; set; }
    }
}
