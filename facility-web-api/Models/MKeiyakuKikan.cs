﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MKeiyakuKikan
    {
        public int CodeKeiyakuKikan { get; set; }
        public string NameKeiyakuKikan { get; set; }
    }
}
