﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class NichijouKihon
    {
        public int TatemonoId { get; set; }
        public int NoNichijou { get; set; }
        public DateTime? NengappiKihyou { get; set; }
        public string NameTourokusha { get; set; }
        public string TeikyoushaJouhou { get; set; }
        public string Naiyou { get; set; }
        public DateTime? NengappiKanryou { get; set; }
        public int? NoJoutai { get; set; }
    }
}
