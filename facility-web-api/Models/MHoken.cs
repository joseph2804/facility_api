﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MHoken
    {
        public int CodeHoken { get; set; }
        public string NameHoken { get; set; }
    }
}
