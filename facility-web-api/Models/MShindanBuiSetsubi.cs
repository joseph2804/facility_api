﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MShindanBuiSetsubi
    {
        public int CodeShindanBuiSetsubi { get; set; }
        public string NameShindanBuiSetsubi { get; set; }
        public string Kikaku { get; set; }
        public int? TaiyouNensu { get; set; }
        public decimal? TankaKoushin { get; set; }
        public int? ShuzenNensu { get; set; }
        public decimal? TankaShuzen { get; set; }
        public int? CodeUnitType { get; set; }
        public int? FlgDel { get; set; }
        public int? CodeSetsubiKubun { get; set; }
    }
}
