﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class RiyouWariate
    {
        public string ShisetsuId { get; set; }
        public int NoWariate { get; set; }
        public string NameWariate { get; set; }
        public string WariateKinou { get; set; }
        public decimal? NobeyukaMenseki { get; set; }
        public decimal? KakakuRiyou { get; set; }
        public string NameRiyoushaMain { get; set; }
        public int? CountKomaRiyouKanou { get; set; }
        public decimal? KakakuGenmen { get; set; }
        public string Bikou { get; set; }
        public int? FlgDel { get; set; }
        public int? Sort { get; set; }
    }
}
