﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MChousaKoumokuSetsubi
    {
        public int CodeChousaKoumokuSetsubi { get; set; }
        public string NoChousaKoumoku { get; set; }
        public double? Omomi { get; set; }
        public string ShindanKoumoku { get; set; }
        public int? FlgDel { get; set; }
        public int? CodeShindanKoumokuSetsubi { get; set; }
        public int? Code12Setsubi { get; set; }
        public int? Flg12 { get; set; }
        public string HanteiKijun { get; set; }
        public int? FlgKeikaku { get; set; }
    }
}
