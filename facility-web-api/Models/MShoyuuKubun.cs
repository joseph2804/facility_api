﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MShoyuuKubun
    {
        public int CodeShoyuuKubun { get; set; }
        public string NameShoyuuKubun { get; set; }
    }
}
