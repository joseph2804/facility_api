﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MJoukasouShurui
    {
        public int CodeJoukasouShurui { get; set; }
        public string NameJoukasouShurui { get; set; }
    }
}
