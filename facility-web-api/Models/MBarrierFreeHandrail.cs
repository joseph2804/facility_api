﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MBarrierFreeHandrail
    {
        public int CodeBarrierFreeHandrail { get; set; }
        public string NameBarrierFreeHandrail { get; set; }
    }
}
