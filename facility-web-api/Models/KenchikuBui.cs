﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class KenchikuBui
    {
        public int TatemonoId { get; set; }
        public int CodeShindanBuiKenchiku { get; set; }
        public decimal? Amount { get; set; }
        public decimal? KakakuShutoku { get; set; }
        public DateTime? NengappiSecchi { get; set; }
    }
}
