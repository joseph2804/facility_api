﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class CHOUSA_KENCHIKU_RIREKI
    {
        public int TATEMONO_ID { get; set; }
        public int NO_CHOUSA { get; set; }
        public int TENKEN_SHUBETSU { get; set; }
        public DateTime NENGAPPI_CHOUSA { get; set; }
        public string NAME_CHOUSAIN { get; set; }
        public int? CODE_CHOUSA_KEKKA { get; set; }
        public string COMMENT_CHOUSA_KEKKA { get; set; }
        public string TAIOU_HOUSHIN { get; set; }
        public decimal? KAKAKU_MITSUMORI { get; set; }
        public int? TENKEN_YOUTEI_NENGATSU { get; set; }
    }
}
