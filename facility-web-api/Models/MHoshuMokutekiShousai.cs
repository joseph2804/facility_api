﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MHoshuMokutekiShousai
    {
        public int CodeHoshuMokutekiShousai { get; set; }
        public int? CodeHoshuMokuteki { get; set; }
        public string NameHoshuMokutekiShousai { get; set; }
    }
}
