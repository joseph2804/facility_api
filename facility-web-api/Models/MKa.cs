﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MKa
    {
        public int CodeKa { get; set; }
        public string NameKa { get; set; }
    }
}
