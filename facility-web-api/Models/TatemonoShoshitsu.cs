﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class TatemonoShoshitsu
    {
        public int TatemonoId { get; set; }
        public int NoShoshitsu { get; set; }
        public int? Kaisu { get; set; }
        public string NoHeya { get; set; }
        public string Youto { get; set; }
        public string Riyousha { get; set; }
        public int? Sort { get; set; }
    }
}
