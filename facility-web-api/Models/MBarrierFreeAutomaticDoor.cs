﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MBarrierFreeAutomaticDoor
    {
        public int CodeBarrierFreeAutomaticDoor { get; set; }
        public string NameBarrierFreeAutomaticDoor { get; set; }
    }
}
