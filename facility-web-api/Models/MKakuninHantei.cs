﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MKakuninHantei
    {
        public int CodeKakuninHantei { get; set; }
        public string NameKakuninHantei { get; set; }
    }
}
