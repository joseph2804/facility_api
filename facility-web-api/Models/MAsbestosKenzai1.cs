﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MAsbestosKenzai1
    {
        public int CodeAsbestosKenzai1 { get; set; }
        public string NameAsbestosKenzai1 { get; set; }
    }
}
