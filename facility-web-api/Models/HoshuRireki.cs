﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class HoshuRireki
    {
        public int TatemonoId { get; set; }
        public int NoHoshu { get; set; }
        public DateTime NengappiHoshu { get; set; }
        public string HoshuKasho { get; set; }
        public string HoshuNaiyou { get; set; }
        public decimal? KakakuHoshu { get; set; }
        public int? CodeHoshuKubun { get; set; }
        public string NameHoshu { get; set; }
        public decimal? KakakuKisai { get; set; }
        public decimal? KakakuHojo { get; set; }
        public DateTime? NengappiHoshuStart { get; set; }
        public DateTime? NengappiHoshuEnd { get; set; }
        public int? CodeHoshuShubetsu { get; set; }
        public int? CodeHoshuBunrui { get; set; }
        public int? CodeYosanKa { get; set; }
        public int? CodeHacchuKa { get; set; }
        public string NoKouji { get; set; }
        public int? FlgTmp { get; set; }
    }
}
