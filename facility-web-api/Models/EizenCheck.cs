﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class EizenCheck
    {
        public string ShisetsuId { get; set; }
        public int NoCheck { get; set; }
        public int NendoCheck { get; set; }
        public int TatemonoId { get; set; }
        public DateTime NengappiSakusei { get; set; }
        public int CodeTantouKa { get; set; }
        public string NameTantousha { get; set; }
        public string NumberPhone { get; set; }
        public string NumberNaisen { get; set; }
        public string HoshuKasho { get; set; }
        public string HoshuNaiyou { get; set; }
        public int? CodeJigyouKubun { get; set; }
        public string HojoNaiyou { get; set; }
        public int? CodeHoshuBunrui { get; set; }
        public string HoshuSeika { get; set; }
        public int? CodeKakuninHantei { get; set; }
        public string CommentKakuninHantei { get; set; }
        public int? CodeKakuninHanteiTmp { get; set; }
        public string CommentKakuninHanteiTmp { get; set; }
        public int? CodeHoshuMokuteki1 { get; set; }
        public int? CodeHoshuMokutekiShousai1 { get; set; }
        public string HoshuMokutekiNaiyou1 { get; set; }
        public int? CodeHoshuMokuteki2 { get; set; }
        public int? CodeHoshuMokutekiShousai2 { get; set; }
        public string HoshuMokutekiNaiyou2 { get; set; }
        public int? CodeHoshuMokuteki3 { get; set; }
        public int? CodeHoshuMokutekiShousai3 { get; set; }
        public string HoshuMokutekiNaiyou3 { get; set; }
        public int? CodeHoshuMokuteki4 { get; set; }
        public int? CodeHoshuMokutekiShousai4 { get; set; }
        public string HoshuMokutekiNaiyou4 { get; set; }
        public int? CodeHoshuMokuteki5 { get; set; }
        public int? CodeHoshuMokutekiShousai5 { get; set; }
        public string HoshuMokutekiNaiyou5 { get; set; }
        public string Bikou { get; set; }
        public int? NendoKaishu { get; set; }
    }
}
