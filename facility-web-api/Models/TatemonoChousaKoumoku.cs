﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class TatemonoChousaKoumoku
    {
        public int TatemonoId { get; set; }
        public int NoNenkanKeikaku { get; set; }
        public int NoChousaKoumoku { get; set; }
        public int? No { get; set; }
        public string Basho { get; set; }
        public string ChousaKoumoku { get; set; }
    }
}
