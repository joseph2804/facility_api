﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MChushajouShubetsu
    {
        public int CodeChushajouShubetsu { get; set; }
        public string NameChushajouShubetsu { get; set; }
    }
}
