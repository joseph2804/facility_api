﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MChousaKekka
    {
        public int CodeChousaKekka { get; set; }
        public string NameChousaKekka { get; set; }
    }
}
