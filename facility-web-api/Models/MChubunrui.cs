﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MChubunrui
    {
        public int CodeChubunrui { get; set; }
        public string NameChubunrui { get; set; }
    }
}
