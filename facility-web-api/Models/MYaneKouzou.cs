﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MYaneKouzou
    {
        public int CodeYaneKouzou { get; set; }
        public string NameYaneKouzou { get; set; }
    }
}
