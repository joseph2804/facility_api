﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MTenkenKoumoku
    {
        public int CodeTenkenKoumoku { get; set; }
        public int? NoTenkenKoumoku { get; set; }
        public int? CodeTenkenBui { get; set; }
        public string TenkenKoumoku { get; set; }
        public string TenkenNaiyou { get; set; }
        public double Omomi { get; set; }
        public int? FlgDel { get; set; }
        public string TenkenHouhou { get; set; }
    }
}
