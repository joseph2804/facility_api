﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MShindanBuiKenchiku
    {
        public int CodeShindanBuiKenchiku { get; set; }
        public string NameShindanBuiKenchiku { get; set; }
        public string Kikaku { get; set; }
        public int? TaiyouNensu { get; set; }
        public decimal? TankaKoushin { get; set; }
        public int? ShuzenNensu { get; set; }
        public decimal? TankaShuzen { get; set; }
        public int? CodeUnitType { get; set; }
        public int? FlgDel { get; set; }
        public int? Code12Kenchiku { get; set; }
    }
}
