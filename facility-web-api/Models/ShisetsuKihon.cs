﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class ShisetsuKihon
    {
        public string ShisetsuId { get; set; }
        public int CodeDaibunrui { get; set; }
        public int? CodeChubunrui { get; set; }
        public int CodeShoubunrui { get; set; }
        public int NoShisetsu { get; set; }
        public int? CodeShisetsuShubetsu { get; set; }
        public int? CodeKa { get; set; }
        public string NameShisetsu { get; set; }
        public string Shozaichi { get; set; }
        public string NameFile { get; set; }
        public int? CodeShisetsuKeitai { get; set; }
        public int? CodeHinansho { get; set; }
        public int? CodeKanriKeitai { get; set; }
        public string KonkyoHourei { get; set; }
        public string ShisetsuGaiyou { get; set; }
        public int? FlgDel { get; set; }
        public int? CodeChiiki { get; set; }
        public int? CodeKaikeiShubetsu { get; set; }
        public string NameShisetsuRyaku { get; set; }
        public string Bikou { get; set; }
        public DateTime? NengappiKyouyou { get; set; }
        public int? CodeYoutoChiiki { get; set; }
        public double? KenpeiRitsu { get; set; }
        public double? YousekiRitsu { get; set; }
        public int? CodeYouto { get; set; }
        public int? CodeBichikuUmu { get; set; }
        public string NumberPhone { get; set; }
        public int? CodeBarrierFreeElevator { get; set; }
        public int? CodeBarrierFreeToilet { get; set; }
        public int? CodeBarrierFreeSlope { get; set; }
        public int? CodeBarrierFreeAutomaticDoor { get; set; }
        public int? CodeBarrierFreeHandrail { get; set; }
        public int? CodeBarrierFreeBrailleBlock { get; set; }
        public DateTime? NengappiKyouyouEnd { get; set; }
        public DateTime? NengappiKanriStart { get; set; }
        public DateTime? NengappiKanriEnd { get; set; }
        public string NameKanrisha { get; set; }
        public string YouYuushikakusha { get; set; }
        public int? CodeChushajouUmu { get; set; }
        public int? CodeChurinjouUmu { get; set; }
        public int? CodeChushajouShubetsu { get; set; }
        public int? CodeChurinjouShubetsu { get; set; }
        public int? CountChushajou { get; set; }
        public int? CountChurinjou { get; set; }
        public int? CountChushajouBarrierFree { get; set; }
        public string KyuukanTeikyuu { get; set; }
        public string KyuukanOther { get; set; }
        public string UneiJikanHeijitsu { get; set; }
        public string UneiJikanKyuujitsu { get; set; }
        public int? CodeJuuyoudo { get; set; }
        public int? CodeDouroKubun { get; set; }
        public string NameDouroKanrisha { get; set; }
        public decimal? WidthDouro { get; set; }
        public decimal? LengthDouro { get; set; }
        public string BikouKanrisha { get; set; }
        public int? CodeShiryouUmu1 { get; set; }
        public string NameShiryou1 { get; set; }
        public string NameShiryouHokan1 { get; set; }
        public int? CodeShiryouUmu2 { get; set; }
        public string NameShiryou2 { get; set; }
        public string NameShiryouHokan2 { get; set; }
        public string BikouBarrierFreeSlope { get; set; }
        public string BikouBarrierFreeBrailleBlock { get; set; }
        public int? CodeKoudoChiku { get; set; }
        public int? Flg12 { get; set; }
        public int? TokutenSoft { get; set; }
        public int? TokutenHard { get; set; }
        public string Moushiokuri { get; set; }
        public string UneiNichiHeijitsu { get; set; }
        public string UneiNichiKyuujitsu { get; set; }
        public string NoZaisan { get; set; }
        public string NumberPhoneKanrisha { get; set; }
        public decimal? Ido { get; set; }
        public decimal? Keido { get; set; }
        public string KeikakuJoutaiKinou { get; set; }
        public string KeikakuJoutaiTatemono { get; set; }
        public string KeikakuHoukouseiKinou { get; set; }
        public string KeikakuHoukouseiTatemono { get; set; }
    }
}
