﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class TatemonoShitsunaiki
    {
        public int TatemonoId { get; set; }
        public int NoShitsunaiki { get; set; }
        public int? Kaisu { get; set; }
        public string NameHeya { get; set; }
        public string NameKataban { get; set; }
        public int? Sort { get; set; }
    }
}
