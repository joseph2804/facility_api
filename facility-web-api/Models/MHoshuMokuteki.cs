﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MHoshuMokuteki
    {
        public int CodeHoshuMokuteki { get; set; }
        public string NameHoshuMokuteki { get; set; }
    }
}
