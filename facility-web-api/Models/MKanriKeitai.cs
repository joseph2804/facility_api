﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MKanriKeitai
    {
        public int CodeKanriKeitai { get; set; }
        public string NameKanriKeitai { get; set; }
    }
}
