﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class TatemonoNenkanKeikaku
    {
        public int TatemonoId { get; set; }
        public int NoNenkanKeikaku { get; set; }
        public int TenkenHouhou { get; set; }
        public string NameGyoumu { get; set; }
        public int? Sort { get; set; }
    }
}
