﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class SousaLog
    {
        public int Id { get; set; }
        public string LoginId { get; set; }
        public DateTime? NichijiKiroku { get; set; }
        public int? CodeLogShubetsu { get; set; }
        public int? CodeSousaShubetsu { get; set; }
        public string CodeTaishou { get; set; }
    }
}
