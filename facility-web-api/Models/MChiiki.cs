﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MChiiki
    {
        public int CodeChiiki { get; set; }
        public string NameChiiki { get; set; }
    }
}
