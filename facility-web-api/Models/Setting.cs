﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class Setting
    {
        public string CodeSetting { get; set; }
        public string ValueSetting { get; set; }
        public string Memo { get; set; }
    }
}
