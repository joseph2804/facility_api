﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MUnitType
    {
        public int CodeUnitType { get; set; }
        public string NameUnitType { get; set; }
    }
}
