﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MHyouka
    {
        public int CodeHyouka { get; set; }
        public string NameHyouka { get; set; }
        public string Memo { get; set; }
        public int? HyoukaTokuten { get; set; }
    }
}
