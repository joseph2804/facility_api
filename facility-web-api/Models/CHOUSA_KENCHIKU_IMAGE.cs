﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class CHOUSA_KENCHIKU_IMAGE
    {
        public int TATEMONO_ID { get; set; }
        public int NO_CHOUSA { get; set; }
        public int NO_UPLOAD { get; set; }
        public string NAME_FILE { get; set; }
        public int? NO_CHOUSA_KOUMOKU { get; set; }
    }
}
