﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MShisetsuShubetsu
    {
        public int CodeShisetsuShubetsu { get; set; }
        public string NameShisetsuShubetsu { get; set; }
    }
}
