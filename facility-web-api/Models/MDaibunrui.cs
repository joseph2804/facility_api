﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MDaibunrui
    {
        public int CodeDaibunrui { get; set; }
        public string NameDaibunrui { get; set; }
    }
}
