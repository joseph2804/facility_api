﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class ChousaKenchikuUpload
    {
        public int TatemonoId { get; set; }
        public int NoChousa { get; set; }
        public int NoUpload { get; set; }
        public string NameFile { get; set; }
        public string Bikou { get; set; }
    }
}
