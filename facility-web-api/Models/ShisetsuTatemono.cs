﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class ShisetsuTatemono
    {
        public int TatemonoId { get; set; }
        public string ShisetsuId { get; set; }
        public int? NoTatemono { get; set; }
        public decimal? NobeyukaMenseki { get; set; }
        public string NameTatemono1 { get; set; }
        public string NameTatemono2 { get; set; }
        public int? KaisuChijou { get; set; }
        public int? KaisuChika { get; set; }
        public string Bikou { get; set; }
        public DateTime? NengappiKyouyou { get; set; }
        public decimal? TochiMenseki { get; set; }
        public int? FlgDaihyou { get; set; }
    }
}
