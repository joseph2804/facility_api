﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class EizenCheckUpload
    {
        public string ShisetsuId { get; set; }
        public int NoCheck { get; set; }
        public int NoUpload { get; set; }
        public string NameFile { get; set; }
        public string Bikou { get; set; }
    }
}
