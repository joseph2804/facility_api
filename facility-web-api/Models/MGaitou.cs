﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MGaitou
    {
        public int CodeGaitou { get; set; }
        public string NameGaitou { get; set; }
    }
}
