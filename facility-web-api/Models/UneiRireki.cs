﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class UneiRireki
    {
        public string ShisetsuId { get; set; }
        public int NenUnei { get; set; }
        public int TsukiUnei { get; set; }
        public int? UneiNissuu { get; set; }
        public int? CountRiyousha { get; set; }
    }
}
