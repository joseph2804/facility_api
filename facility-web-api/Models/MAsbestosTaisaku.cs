﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MAsbestosTaisaku
    {
        public int CodeAsbestosTaisaku { get; set; }
        public string NameAsbestosTaisaku { get; set; }
    }
}
