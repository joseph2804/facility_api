﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class LccSim
    {
        public string ShisetsuId { get; set; }
        public int TatemonoId { get; set; }
        public int Nendo { get; set; }
        public int Shubetsu { get; set; }
        public DateTime? NengappiKenchiku { get; set; }
        public decimal? NobeyukaMenseki { get; set; }
        public int? Nendo2 { get; set; }
        public decimal? Kingaku { get; set; }
    }
}
