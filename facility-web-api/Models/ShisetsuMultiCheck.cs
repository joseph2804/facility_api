﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class ShisetsuMultiCheck
    {
        public string ShisetsuId { get; set; }
        public string NameColumn { get; set; }
        public int CodeChecked { get; set; }
    }
}
