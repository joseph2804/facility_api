﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MAsbestosTaisakuHouhou
    {
        public int CodeAsbestosTaisakuHouhou { get; set; }
        public string NameAsbestosTaisakuHouhou { get; set; }
    }
}
