﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MHoshuShubetsu
    {
        public int CodeHoshuShubetsu { get; set; }
        public string NameHoshuShubetsu { get; set; }
    }
}
