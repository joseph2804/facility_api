﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class ShisetsuTochi
    {
        public string ShisetsuId { get; set; }
        public int TochiId { get; set; }
        public decimal? TochiMenseki { get; set; }
        public string Bikou { get; set; }
    }
}
