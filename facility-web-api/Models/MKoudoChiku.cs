﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MKoudoChiku
    {
        public int CodeKoudoChiku { get; set; }
        public string NameKoudoChiku { get; set; }
    }
}
