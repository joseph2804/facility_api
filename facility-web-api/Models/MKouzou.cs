﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MKouzou
    {
        public int CodeKouzou { get; set; }
        public string NameKouzou { get; set; }
        public string Kigou { get; set; }
    }
}
