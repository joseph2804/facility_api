﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MSetsubiKubun
    {
        public int CodeSetsubiKubun { get; set; }
        public string NameSetsubiKubun { get; set; }
    }
}
