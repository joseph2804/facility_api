﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class TatemonoShitsugaiki
    {
        public int TatemonoId { get; set; }
        public int NoShitsugaiki { get; set; }
        public string KeitouKigou { get; set; }
        public string Keitoumei { get; set; }
        public string SecchiBasho { get; set; }
        public string KikiKeishiki { get; set; }
        public string UntenJikan { get; set; }
        public int? Sort { get; set; }
    }
}
