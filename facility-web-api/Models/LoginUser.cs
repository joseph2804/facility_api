﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class LoginUser
    {
        public string LoginId { get; set; }
        public string Password { get; set; }
        public int FlgKengen { get; set; }
        public int FlgKanri { get; set; }
        public int FlgTatemono { get; set; }
        public int FlgTochi { get; set; }
        public int FlgShisetsu { get; set; }
        public int? CodeKa { get; set; }
        public int? FlgEizen { get; set; }
    }
}
