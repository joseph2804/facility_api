﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class NichijouRireki
    {
        public int TatemonoId { get; set; }
        public int NoNichijou { get; set; }
        public int NoTaiou { get; set; }
        public DateTime? NengappiTaiou { get; set; }
        public string TaiyouShoukuin { get; set; }
        public string TaiouNaiyou { get; set; }
    }
}
