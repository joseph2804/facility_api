﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MShindanKoumokuKenchiku
    {
        public int CodeShindanKoumokuKenchiku { get; set; }
        public string NameShindanKoumokuKenchiku { get; set; }
        public int? CodeHyoukaBui { get; set; }
    }
}
