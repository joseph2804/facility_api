﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MKaikeiShubetsu
    {
        public int CodeKaikeiShubetsu { get; set; }
        public string NameKaikeiShubetsu { get; set; }
    }
}
