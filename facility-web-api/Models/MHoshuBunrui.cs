﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MHoshuBunrui
    {
        public int CodeHoshuBunrui { get; set; }
        public string NameHoshuBunrui { get; set; }
    }
}
