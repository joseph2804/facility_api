﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MTaishinShindan
    {
        public int CodeTaishinShindan { get; set; }
        public string NameTaishinShindan { get; set; }
    }
}
