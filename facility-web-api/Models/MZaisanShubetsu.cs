﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MZaisanShubetsu
    {
        public int CodeZaisanShubetsu { get; set; }
        public string NameZaisanShubetsu { get; set; }
    }
}
