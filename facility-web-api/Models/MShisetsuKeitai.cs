﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MShisetsuKeitai
    {
        public int CodeShisetsuKeitai { get; set; }
        public string NameShisetsuKeitai { get; set; }
    }
}
