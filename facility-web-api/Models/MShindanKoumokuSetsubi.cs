﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MShindanKoumokuSetsubi
    {
        public int CodeShindanKoumokuSetsubi { get; set; }
        public string NameShindanKoumokuSetsubi { get; set; }
        public int? CodeSetsubiBunrui { get; set; }
        public int? CodeHyoukaBui { get; set; }
    }
}
