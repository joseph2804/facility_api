﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class CostRireki
    {
        public string ShisetsuId { get; set; }
        public int NenCost { get; set; }
        public int TsukiCost { get; set; }
        public decimal? KakakuJinkenhiIji { get; set; }
        public string BikouJinkenhiIji { get; set; }
        public decimal? KakakuJousuidouIji { get; set; }
        public string BikouJousuidouIji { get; set; }
        public decimal? AmountJousuidouIji { get; set; }
        public decimal? KakakuGesuidouIji { get; set; }
        public string BikouGesuidouIji { get; set; }
        public decimal? AmountGesuidouIji { get; set; }
        public decimal? KakakuGasuIji { get; set; }
        public string BikouGasuIji { get; set; }
        public decimal? AmountGasuIji { get; set; }
        public decimal? KakakuDenkiIji { get; set; }
        public string BikouDenkiIji { get; set; }
        public decimal? AmountDenkiIji { get; set; }
        public decimal? KakakuKounetsuOtherIji { get; set; }
        public string BikouKounetsuOtherIji { get; set; }
        public decimal? KakakuShuzenIji { get; set; }
        public string BikouShuzenIji { get; set; }
        public decimal? KakakuJuyouhiOtherIji { get; set; }
        public string BikouJuyouhiOtherIji { get; set; }
        public decimal? KakakuEkimuhiIji { get; set; }
        public string BikouEkimuhiIji { get; set; }
        public decimal? KakakuItakuryouIji { get; set; }
        public string BikouItakuryouIji { get; set; }
        public decimal? KakakuChinshakuryouIji { get; set; }
        public string BikouChinshakuryouIji { get; set; }
        public decimal? KakakuUkeoiIji { get; set; }
        public string BikouUkeoiIji { get; set; }
        public decimal? KakakuBihinIji { get; set; }
        public string BikouBihinIji { get; set; }
        public decimal? KakakuOtherIji { get; set; }
        public string BikouOtherIji { get; set; }
        public decimal? KakakuJinkenhiJigyou { get; set; }
        public string BikouJinkenhiJigyou { get; set; }
        public decimal? KakakuJuyouhiOtherJigyou { get; set; }
        public string BikouJuyouhiOtherJigyou { get; set; }
        public decimal? KakakuEkimuhiJigyou { get; set; }
        public string BikouEkimuhiJigyou { get; set; }
        public decimal? KakakuItakuryouJigyou { get; set; }
        public string BikouItakuryouJigyou { get; set; }
        public decimal? KakakuChinshakuryouJigyou { get; set; }
        public string BikouChinshakuryouJigyou { get; set; }
        public decimal? KakakuBihinJigyou { get; set; }
        public string BikouBihinJigyou { get; set; }
        public decimal? KakakuOtherJigyou { get; set; }
        public string BikouOtherJigyou { get; set; }
        public decimal? KakakuRiyouryouShunyu { get; set; }
        public string BikouRiyouryouShunyu { get; set; }
        public decimal? KakakuShiyouryouShunyu { get; set; }
        public string BikouShiyouryouShunyu { get; set; }
        public decimal? KakakuJigyousankaShunyu { get; set; }
        public string BikouJigyousankaShunyu { get; set; }
        public decimal? KakakuOtherShunyu { get; set; }
        public string BikouOtherShunyu { get; set; }
        public decimal? KakakuShiteiKanriryou { get; set; }
        public string BikouShiteiKanriryou { get; set; }
        public decimal? KakakuGenkaShoukyaku { get; set; }
        public string BikouGenkaShoukyaku { get; set; }
        public decimal? ShokuinSeiki1 { get; set; }
        public decimal? ShokuinSeiki2 { get; set; }
        public decimal? ShokuinOther1 { get; set; }
        public decimal? ShokuinOther2 { get; set; }
    }
}
