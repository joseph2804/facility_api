﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MGasuShurui
    {
        public int CodeGasuShurui { get; set; }
        public string NameGasuShurui { get; set; }
    }
}
