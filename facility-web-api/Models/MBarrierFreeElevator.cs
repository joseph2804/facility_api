﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MBarrierFreeElevator
    {
        public int CodeBarrierFreeElevator { get; set; }
        public string NameBarrierFreeElevator { get; set; }
    }
}
