﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class M12Setsubi
    {
        public int Code12Setsubi { get; set; }
        public string Name12Setsubi { get; set; }
        public int? CodeSetsubiBunrui { get; set; }
    }
}
