﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MHoshuKubun
    {
        public int CodeHoshuKubun { get; set; }
        public string NameHoshuKubun { get; set; }
    }
}
