﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class TatemonoMultiCheck
    {
        public int TatemonoId { get; set; }
        public string NameColumn { get; set; }
        public int CodeChecked { get; set; }
    }
}
