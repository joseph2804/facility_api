﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MBarrierFree
    {
        public int CodeBarrierFree { get; set; }
        public string NameBarrierFree { get; set; }
    }
}
