﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MSuidouShurui
    {
        public int CodeSuidouShurui { get; set; }
        public string NameSuidouShurui { get; set; }
    }
}
