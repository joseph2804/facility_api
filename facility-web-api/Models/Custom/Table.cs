﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace facility_web_api.Models.Custom
{
    public class Table
    {
        public string TABLE_CATALOG { get; set; }
        public string TABLE_SCHEMA { get; set; }
        public string TABLE_NAME { get; set; }
        //public string TABLE_NAME { get; set; }
    }
}
