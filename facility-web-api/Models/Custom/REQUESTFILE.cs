﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace facility_web_api.Models.Custom
{
    public class REQUESTFILE
    {
        public IFormFile File { get; set; }
    }
}
