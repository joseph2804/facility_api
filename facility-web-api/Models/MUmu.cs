﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MUmu
    {
        public int CodeUmu { get; set; }
        public string NameUmu { get; set; }
    }
}
