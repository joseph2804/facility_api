﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MLogShubetsu
    {
        public int CodeLogShubetsu { get; set; }
        public string NameLogShubetsu { get; set; }
    }
}
