﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class M12Kenchiku
    {
        public int Code12Kenchiku { get; set; }
        public string Name12Kenchiku { get; set; }
    }
}
