﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MBarrierFreeBrailleBlock
    {
        public int CodeBarrierFreeBrailleBlock { get; set; }
        public string NameBarrierFreeBrailleBlock { get; set; }
    }
}
