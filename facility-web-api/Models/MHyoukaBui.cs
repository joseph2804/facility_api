﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MHyoukaBui
    {
        public int CodeHyoukaBui { get; set; }
        public string NameHyoukaBui { get; set; }
        public string RekkaChousaKoumoku { get; set; }
        public decimal? JuyouKeisu { get; set; }
        public int? FlgTaishou { get; set; }
    }
}
