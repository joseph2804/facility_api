﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class TatemonoTochi
    {
        public int TochiId { get; set; }
        public int TatemonoId { get; set; }
        public decimal? TochiMenseki { get; set; }
        public string Bikou { get; set; }
    }
}
