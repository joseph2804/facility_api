﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class TochiKihon
    {
        public int TochiId { get; set; }
        public string Chiban { get; set; }
        public string Shozaichi { get; set; }
        public decimal? TochiMenseki { get; set; }
        public int? CodeShoyuuKubun { get; set; }
        public string Bikou { get; set; }
        public int? FlgDel { get; set; }
        public int? CodeTochiBunrui { get; set; }
        public decimal? TochiMensekiTouki { get; set; }
        public decimal? KakakuHyouka { get; set; }
        public int? CodeYoutoChiiki { get; set; }
        public double? KenpeiRitsu { get; set; }
        public double? YousekiRitsu { get; set; }
        public decimal? KakakuChinshaku { get; set; }
        public string BikouKanrisha { get; set; }
        public int? CodeKa { get; set; }
        public string Moushiokuri { get; set; }
        public string NoZaisan { get; set; }
        public string EdaZaisan { get; set; }
    }
}
