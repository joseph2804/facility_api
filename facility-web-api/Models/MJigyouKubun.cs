﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MJigyouKubun
    {
        public int CodeJigyouKubun { get; set; }
        public string NameJigyouKubun { get; set; }
    }
}
