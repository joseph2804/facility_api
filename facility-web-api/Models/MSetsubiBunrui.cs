﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MSetsubiBunrui
    {
        public int CodeSetsubiBunrui { get; set; }
        public string NameSetsubiBunrui { get; set; }
        public int? CodeSetsubiKubun { get; set; }
        public int? Flg12 { get; set; }
    }
}
