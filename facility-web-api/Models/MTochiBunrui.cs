﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MTochiBunrui
    {
        public int CodeTochiBunrui { get; set; }
        public string NameTochiBunrui { get; set; }
    }
}
