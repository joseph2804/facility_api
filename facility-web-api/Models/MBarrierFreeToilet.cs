﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MBarrierFreeToilet
    {
        public int CodeBarrierFreeToilet { get; set; }
        public string NameBarrierFreeToilet { get; set; }
    }
}
