﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class EizenCheckBuiSetsubi
    {
        public string ShisetsuId { get; set; }
        public int NoCheck { get; set; }
        public int CodeShindanBuiSetsubi { get; set; }
    }
}
