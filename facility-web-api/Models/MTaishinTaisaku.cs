﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MTaishinTaisaku
    {
        public int CodeTaishinTaisaku { get; set; }
        public string NameTaishinTaisaku { get; set; }
    }
}
