﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class UneiRirekiRiyouWariate
    {
        public string ShisetsuId { get; set; }
        public int NoWariate { get; set; }
        public int NenUnei { get; set; }
        public int TsukiUnei { get; set; }
        public int? CountRiyousha { get; set; }
        public int? CountKomaRiyou { get; set; }
        public decimal? KakakuShunyuRiyou { get; set; }
    }
}
