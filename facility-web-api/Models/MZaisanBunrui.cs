﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MZaisanBunrui
    {
        public int CodeZaisanBunrui { get; set; }
        public string NameZaisanBunrui { get; set; }
    }
}
