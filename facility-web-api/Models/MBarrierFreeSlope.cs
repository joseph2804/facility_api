﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MBarrierFreeSlope
    {
        public int CodeBarrierFreeSlope { get; set; }
        public string NameBarrierFreeSlope { get; set; }
    }
}
