﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MShoubunrui
    {
        public int CodeShoubunrui { get; set; }
        public string NameShoubunrui { get; set; }
    }
}
