﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MKeiyakuHouhou
    {
        public int CodeKeiyakuHouhou { get; set; }
        public string NameKeiyakuHouhou { get; set; }
    }
}
