﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MAsbestosKenzai2
    {
        public int CodeAsbestosKenzai2 { get; set; }
        public string NameAsbestosKenzai2 { get; set; }
    }
}
