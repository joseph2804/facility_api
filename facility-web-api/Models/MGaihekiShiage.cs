﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MGaihekiShiage
    {
        public int CodeGaihekiShiage { get; set; }
        public string NameGaihekiShiage { get; set; }
    }
}
