﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MAsbestosShurui
    {
        public int CodeAsbestosShurui { get; set; }
        public string NameAsbestosShurui { get; set; }
    }
}
