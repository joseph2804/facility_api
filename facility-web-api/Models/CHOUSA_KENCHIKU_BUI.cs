﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class CHOUSA_KENCHIKU_BUI
    {
        public int TATEMONO_ID { get; set; }
        public int NO_CHOUSA { get; set; }
        public int NO_NENKAN_KEIKAKU { get; set; }
        public int NO_CHOUSA_KOUMOKU { get; set; }
        public int? NO_JOUTAI { get; set; }
        public string SHASHIN { get; set; }
        public string BIKO { get; set; }
    }
}
