﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MChousaKoumokuKenchiku
    {
        public int CodeChousaKoumokuKenchiku { get; set; }
        public string NoChousaKoumoku { get; set; }
        public double? Omomi { get; set; }
        public string ShindanKoumoku { get; set; }
        public int? Flg12 { get; set; }
        public int? FlgDel { get; set; }
        public int? CodeShindanKoumokuKenchiku { get; set; }
        public int? Code12Kenchiku { get; set; }
        public string HanteiKijun { get; set; }
        public int? FlgKeikaku { get; set; }
    }
}
