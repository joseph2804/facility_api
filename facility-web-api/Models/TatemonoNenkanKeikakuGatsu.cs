﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class TatemonoNenkanKeikakuGatsu
    {
        public int TatemonoId { get; set; }
        public int NoNenkanKeikaku { get; set; }
        public int Month { get; set; }
    }
}
