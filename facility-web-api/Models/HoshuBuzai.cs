﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class HoshuBuzai
    {
        public int TatemonoId { get; set; }
        public int NoHoshu { get; set; }
        public int CodeHoshuBuzai { get; set; }
    }
}
