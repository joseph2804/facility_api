﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class ItakuDaichou
    {
        public string ShisetsuId { get; set; }
        public int NoItaku { get; set; }
        public int? CodeItakuGyoumu { get; set; }
        public string NameKeiyakusha { get; set; }
        public decimal? KakakuKeiyaku { get; set; }
        public int? CodeKeiyakuHouhou { get; set; }
        public int? NendoKeiyaku { get; set; }
        public int? CodeKeiyakuKikan { get; set; }
        public string Bikou { get; set; }
        public int? Sort { get; set; }
        public int? FlgDel { get; set; }
        public string TextItakuGyoumu { get; set; }
    }
}
