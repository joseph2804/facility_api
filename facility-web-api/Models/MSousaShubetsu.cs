﻿using System;
using System.Collections.Generic;

namespace facility_web_api.Models
{
    public partial class MSousaShubetsu
    {
        public int CodeSousaShubetsu { get; set; }
        public string NameSousaShubetsu { get; set; }
    }
}
